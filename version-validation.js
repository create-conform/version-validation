/////////////////////////////////////////////////////////////////////////////////////////////
//
// version-validation
//
//    Library for validating semantic version strings.
//
// License
//    Apache License Version 2.0
//
// Copyright Nick Verlinden (info@createconform.com)
//
///////////////////////////////////////////////////////////////////////////////////////////// 
/////////////////////////////////////////////////////////////////////////////////////////////
//
// Privates
//
/////////////////////////////////////////////////////////////////////////////////////////////
var validate = require("validate");

/////////////////////////////////////////////////////////////////////////////////////////////
//
// VersionValidator class
//
/////////////////////////////////////////////////////////////////////////////////////////////
function VersionValidator() {
    var self = this;

    this.MAJOR = "version-property-major";
    this.MINOR = "version-property-minor";
    this.PATCH = "version-property-patch";

    function countNumberParts(str) {
        var parts = str.split(".");
        var numbers = 0;
        for (var p = parts.length; p > 0; p--) {
            if (isNaN(parts[p - 1])) {
                break;
            }
            numbers++;
        }
        return numbers;
    }

    this.hasMajor = function(str) {
        return countNumberParts(str) >= 1;
    };
    this.hasMinor = function(str) {
        return countNumberParts(str) >= 2;
    };
    this.hasPatch = function(str) {
        return countNumberParts(str) >= 3;
    };

    // validator
    this.getProperties = function(str) {
        var props = [];
        var numbers = countNumberParts(str);
        if (numbers >= 3) {
            props.push(self.PATCH);
        }
        if (numbers >= 2) {
            props.push(self.MINOR);
        }
        if (numbers >= 1) {
            props.push(self.MAJOR);
        }
        return props;
    };
    this.isValid = function(str) {
        return Object.prototype.toString.call(str) === "[object String]";
    };
}
// set validator prototype
VersionValidator.prototype = validate.Validator;

/////////////////////////////////////////////////////////////////////////////////////////////
module.exports = new VersionValidator();